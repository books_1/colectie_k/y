# Y

## Content

```
./Yan Lianke:
Yan Lianke - In slujba poporului 1.0 '{Literatura}.docx
Yan Lianke - Zile, luni, ani 1.0 '{Literatura}.docx

./Yannick Haenel:
Yannick Haenel - Apara-ti coroana 1.0 '{Literatura}.docx

./Yann Martel:
Yann Martel - Viata lui Pi 2.0 '{Tineret}.docx

./Yann Queffelec:
Yann Queffelec - Nunta nemiloasa 0.9 '{Literatura}.docx

./Yasmina Khadra:
Yasmina Khadra - Sirenele Bagdadului 1.0 '{Literatura}.docx

./Yasuko Thanh:
Yasuko Thanh - Mireasma misterioasa a muntilor galbeni 1.0 '{Literatura}.docx

./Yasunari Kawabata:
Yasunari Kawabata - Dansatoarea din Izu 0.99 '{Literatura}.docx
Yasunari Kawabata - Frumoasele adormite 0.99 '{Literatura}.docx
Yasunari Kawabata - Kyoto sau tinerii indragostiti din stravechiul oras imperial 1.0 '{Literatura}.docx
Yasunari Kawabata - Tara zapezilor 1.0 '{Literatura}.docx
Yasunari Kawabata - Vuietul muntelui 0.9 '{Literatura}.docx

./Yasushi Inoue:
Yasushi Inoue - Cupa de clestar 0.9 '{Literatura}.docx
Yasushi Inoue - Maestrul de ceai 1.0 '{Literatura}.docx
Yasushi Inoue - Nobila doamna din Yodo 1.0 '{Literatura}.docx
Yasushi Inoue - Pusca de vanatoare 1.0 '{Literatura}.docx

./Yoko Ogawa:
Yoko Ogawa - Hotel Iris 1.0 '{Literatura}.docx

./Yram:
Yram - Secretele lumilor astrale 0.9 '{Spiritualitate}.docx

./Yrsa Sigurdardottir:
Yrsa Sigurdardottir - Cenusa si pulbere 1.0 '{Thriller}.docx
Yrsa Sigurdardottir - Suflete damnate 2.0 '{Thriller}.docx
Yrsa Sigurdardottir - Zile intunecate 1.0 '{Thriller}.docx

./Yukio Mishima:
Yukio Mishima - Amurgul marinarului 1.1 '{Literatura}.docx
Yukio Mishima - Confesiunile unei masti 1.0 '{Literatura}.docx
Yukio Mishima - Dupa banchet 0.8 '{Literatura}.docx
Yukio Mishima - Sete de iubire 1.0 '{Literatura}.docx
Yukio Mishima - Tumultul valurilor 1.0 '{Literatura}.docx

./Yuval Noah Harari:
Yuval Noah Harari - 21 de lectii pentru secolul XXI 1.0 '{Istorie}.docx
Yuval Noah Harari - Sapiens. Scurta istorie a omenirii 1.0 '{Istorie}.docx

./Yves Cousteau:
Yves Cousteau - In cautarea Atlantidei 1.0 '{Calatorii}.docx

./Yves Gandon:
Yves Gandon - Capitanul Lafortune 1.0 '{CapasiSpada}.docx
Yves Gandon - Domnul Miracle 1.0 '{Dragoste}.docx
Yves Gandon - Inflacarata Egle 1.0 '{Dragoste}.docx
Yves Gandon - Zulme 1.0 '{Dragoste}.docx

./Yves Navarre:
Yves Navarre - Iedera 1.0 '{Dragoste}.docx

./Yvonne Whittal:
Yvonne Whittal - Pe urmele unei legende 1.0 '{Romance}.docx
Yvonne Whittal - Soimul de argint 1.0 '{Romance}.docx
```

